---
marp: true
theme: bhack
---

# Ballarat Hackerspace


# Raspberry Pi Workshop

---

# Let's go!



---
# Facilities

- Toilets
- Coffee / Tea available
- In case of emergency
    - Follow Instructions
    - Meeting Location


---
# Raspberry Pi

- A single board computer, released in the UK originally in 2012
- Lots of models now!
    - Raspberry Pi 3 Model B+ (what we will be using)
    - Raspberry Pi 4 Model B (with up to 8GB of ram!)
    - Pi Zero (much smaller, less power usage, cheaper)
    - Pico (microcontroller, not full computer, very cheap)
- See https://en.wikipedia.org/wiki/Raspberry_Pi#Series_and_generations


---
# Format for Training
- Several small projects to introduce new concepts
- Feel free to experiment
- Ask questions as we go through!


---
# The Ballarat Hackerspace
- Group of volunteers who are making things, learning and upskilling, and teaching the community
- Non-profit organisation
- Created in 2014
- Memberships are $30/mo for casual (other options too!)
- We also take sponsorships!

---
# Who are you?

- And who am I
- What do you make/build/hack?
- What programming experience do you have?

---
# Outline for Training

1. Learn to run code
2. Basics of Electronics
3. Controlling circuits with code
4. Temperature Sensor Display Project
5. Distance sensor
6. Servo Motors


---
# Your Pi and You

![Raspberry Pi 3B+](images/rpi3b_plus.png)

Things to note: GPIO pins, Camera slot, power, HDMI, USB, Ethernet

---
# Operating Systems

- There are many operating systems for Pis
- We will use Raspbian, a linux/debian variant
- You can download from https://www.raspberrypi.com/software/
- Strongly recommend doing at least once, but it takes too long for this workshop

---
# Running Code

- We will use Python for this workshop
- Easy to use, lots and lots of functionality
- Special libraries exist for the RPi in Python
- Not limited to Python for RPis, as it is a full computer!
- Note the GPIO stuff is specific to the Pi though

---
# Basics of Python
- Whitespace is critical
- Code blocks are determined by indentation
- 4 space indentation levels is the standards
- Python 2 vs 3 - go with 3!
- Great "all round language", widely used


---
# Basic Python Program

```python
# Anything in grey starting with a # is a comment - you don't need to type it!

# Import the random and time modules to get more functions
import time
import random


# Choose a random number (integer) between 5 and 10
start = random.randint(5, 10)

# Count from 0 to start (doesn't include start!)
for i in range(start):
    # These lines must be indented to run in the loop
    value = start - i
    print(value, "...")
    time.sleep(1)  # Wait one second then keep going

# This line is not indented, so runs after the loop finishes
print("Liftoff!")

```

---
# Running your script

- Python scripts are just text files with python in them and a `.py` extension
- Run from the command line: `python3 scriptname.py`
- Run from a program like Thonny

Get the previous script running using both approaches.

---
# User input

```python
your_name = input("Enter your name: ")
print("Hello ", your_name, "!", sep="")
age = int(input("Enter your age:"))
year = 2023
print("You are about", (year - age), "years old.")
```

Give this a go!


---
# Section 3: Basics of Electronics

- Electricity flows in circles; from the power source (+ve), through components, back to power source (-ve)
- It gets *way* more complicated, but you can do lots with basic wiring


---
# Raspberry Pi Pinout

![Raspberry Pi Pinout](images/raspberry_pi_circuit_note_fig2.webp)


---
# Breadboard

![Breadboard](images/breadboard.jpg)



---
# Basic LED circuit

Give this a go: wire up this circuit that turns on an LED

![Dumb LED](images/led_no_smarts.png)


---
# Button

Give this a go: wire up this circuit that turns on the LED with a button press


---
# Pull-up and pull-down resistors

- A basic necessity in many circuits
- "floating" voltage makes it look like the button was pressed
- A pull-down resistor pulls this down to 0V, so these little fluctuations are ignored


---
# Section 4: Controlling circuits with code

We can use python to:
- read input signals from GPIO pins
- turn on or off the output voltage from GPIO pins
- more complicated things like reading protocols and analog input/output

---

![pinout](images/rp2_pinout.png)

---
# Flashing LED with code

```python
# Remember you don't need to type the comments!
import time
import RPi.GPIO as GPIO

output_pin = 7 # pin to configure and use as an output

# set the mode of the GPIO pin to an output
GPIO.setmode(GPIO.BOARD)
GPIO.setup(output_pin, GPIO.OUT)

# loop 10 times, turning light on and off each loop
for i in range(10):
    # turn on
    GPIO.output(output_pin, True)  
    # wait 2 seconds
    time.sleep(2)                  
    # turn off 
    GPIO.output(output_pin, False) 
    # wait 2 seconds
    time.sleep(2)                  
    
GPIO.cleanup()
print("Done!")
```

---
# Input from user

```python
import RPi.GPIO as GPIO
 
output_pin = 7
input_pin = 16
 
GPIO.setmode(GPIO.BOARD)
GPIO.setup(output_pin, GPIO.OUT)
GPIO.setup(input_pin, GPIO.IN)
 
while True:
    if GPIO.input(input_pin):
        GPIO.output(output_pin, True)
    else:
        GPIO.output(output_pin, False)

GPIO.cleanup()

```


---
# Exercise

1. When the button is pressed, blink the LED three times quickly

## Extended exercises
2. Using the reference below[1] wait for the button to be pressed, rather than loop (see `wait_for_edge`)
3. Using the same reference, run a callback function when the button is released.

Using these techniques, you can have your program do other things while it waits for the button!

[1] https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/


---
# Sensors - Temperature and Humidity

![AHT20](images/aht20.png)

Usings the I2C protocol for comms.

- VDD: 5V+
- GND: Ground
- SDA: I2C data pin - connect to SDA on Pi
- SCL: I2C clock pin - connect to SCL on Pi

I've created a breakout wiring harness for you! Grab one and get setup!

---

## Jumping into the deep end

```bash
git clone https://github.com/Chouffy/python_sensor_aht20
sudo apt install python3-smbus
pip3 install smbus2

sudo raspi-config
```

After the last command, in Interface, turn on I2C then click Finish.

Get code and an example from this open source project:
`https://github.com/Chouffy/python_sensor_aht20`

Get this going, have it repeat forever (google: `python while true`) and print out the temperature and humidity nicely (google `python string format`).

**Harder**
Create a log file tracking the temp/humidity every minute, saved to a file.

Thanks to Github user Chouffy!

---
# Sensors - Distance

Grab the script from here: https://pimylifeup.com/raspberry-pi-distance-sensor/
(Step 4)


Note: approximate speed of ultrasonic sound is 34,300 cm/s

---
![](images/Distance-Sensor-Fritz.webp)


---

## Let's go


Those are 1k Ω and 2k Ω resistors - which is which?

1. Setup a script that prints "Found you" whenever something is less than 10cm away.
2. Wrap most of the code into one easy to use function `get_distance`, to make the code easier to use.


**Harder**
1. Remove the 'bouncing' - ensure the thing is lses than 10cm away for at least 2 seconds
2. You can write a module! Put that function into a separate file called `distance.py`, and import it like this:
`from distance import get_distance`

Thanks: https://pimylifeup.com/raspberry-pi-distance-sensor/

---
# Servo Wiring
![Wiring Servo](images/SG90-Datasheet.gif)

Based on http://reefwingrobotics.blogspot.com/2017/02/raspberry-pi-and-towerpro-sg90-micro.html

I just ran directly off 5V, but a separate power supply might be a good idea

---
# Servo Coding

```python
import time
import RPi.GPIO as GPIO

duty = 7.5  # Should be the centre

servo_pin = 32  # Ensure this is a "PWM" enabled pin
GPIO.setmode(GPIO.BOARD)
GPIO.setup(servo_pin, GPIO.OUT)

# Create PWM channel on the servo pin with a frequency of 50Hz
servo = GPIO.PWM(servo_pin, 50)
servo.start(duty)

try:
    for duty in range(5, 11):
        servo.ChangeDutyCycle(duty)
finally:
    GPIO.cleanup()
```

Try get this to keep "scanning".
To reduce jitter, see https://ben.akrin.com/raspberry-pi-servo-jitter/

---
# Next steps

Create a radar!

1. Attach the distance sensor to the servo so it swings around on the arm.
2. Have the arm go in a sweeping motion
3. Have it stop at anything within 15cm and print "found you!"

---
# Thanks for coming!

Consider joining as a member to continue on!

These slides are available free and open source from https://gitlab.com/ballarat-hackerspace/workshops/raspberrypi_workshop/-/blob/master/notes/001intro.pdf

(if that's too hard to remember, go to wiki.ballarathackerspace.org.au)